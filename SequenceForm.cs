using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// TestStand Core API 
using NationalInstruments.TestStand.Interop.API;

// TestStand User Interface Controls
using NationalInstruments.TestStand.Interop.UI;
using NationalInstruments.TestStand.Interop.UI.Support;

// .net specific functions for use with TestStand APIs (TSUtil)
using NationalInstruments.TestStand.Utility;

namespace TestExec
{
    /// <summary>
        /// Summary description for MainForm.
        /// </summary>
        
    partial class SequenceForm : System.Windows.Forms.Form
        {
            private NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr axApplicationMgr;

            // Values that identifies a tag page in this application. Because these values can be converted to numbers, we can store them
            // in a TestStand property. We use this to attach a new numeric property to each TestStand execution object to store which tab page 
            // the application displays for that execution.
            enum TabPageIdentifier
            {
                NotATab,
                SequenceFile,
                Execution,
                Report
            };

            // global flag that indicates if we are programmatically changing the active tab page
            private bool programmaticallyUpdatingTabPages = true;

            private const int WM_QUERYENDSESSION = 0x11;

            // flag that will be set to true if the user tries to shut down windows
            private bool sessionEnding = false;

            // added by Nelson , June 13th.
            private Engine thisEngine;
            User CurrUser;
            string loginname = null;

        /// </summary>
        // a localizer converts text in .NET menus and Forms to the current language by obtaining the translations from the TestStand string resource files
        private Localizer localizer;
           

            private string errorDlgTitle = "Error"; // localized in MainForm_Load

            public SequenceForm(Form mdiParent, NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr appMgr, object executionOrSequenceFile)
            {
                this.axApplicationMgr = appMgr;
                this.MdiParent = mdiParent; // must do this before InitializeComponent. See Note 2 at the top of MainForm.cs

                // Required for Windows Form Designer support
                InitializeComponent();
                                              
                this.Icon = Properties.Resources.testexec;

                if (executionOrSequenceFile is SequenceFile)
                {
                    this.axSequenceFileViewMgr.SequenceFile = executionOrSequenceFile as SequenceFile;
                    DisplaySequenceFile();
                    this.Icon = new System.Drawing.Icon(axApplicationMgr.GetEngine().GetTestStandPath(TestStandPaths.TestStandPath_TestStand) + "\\Components\\Icons\\ni_seqfile.ico");
                }
                else
                {
                    this.axExecutionViewMgr.Execution = executionOrSequenceFile as Execution;
                    DisplayExecution();
                }
                try
                {

                    // this application allows setting of breakpoints on sequences files, so let them persist
                    this.axApplicationMgr.GetEngine().PersistBreakpoints = true;
                    
                    //////////////////////////////////////////////////////////////////////////////////////////
                    // added by Nelson. If current user is operator, turn off display of variables and sequence.
                    /////////////////////////////////////////////////////////////////////////////////////////////
                    thisEngine = this.axApplicationMgr.GetEngine();
                    CurrUser = thisEngine.CurrentUser;
                    loginname = CurrUser.LoginName;
                    if (!String.Equals(loginname, "Operator"))
                    {
                        this.axSequenceFileViewMgr.ConnectVariables(this.axFileVariables);
                        this.axSequenceFileViewMgr.ConnectSequenceList(this.axSequencesList).SetColumnVisible(SeqListConnectionColumns.SeqListConnectionColumn_Comments, true);
                    }
                // connect controls on the Sequence File tab
                    this.axSequenceFileViewMgr.ConnectSequenceView(this.axFileSteps);
                    this.axSequenceFileViewMgr.ConnectCommand(this.axEntryPoint1Button, CommandKinds.CommandKind_ExecutionEntryPoints_Set, 0, CommandConnectionOptions.CommandConnection_NoOptions);
                    this.axSequenceFileViewMgr.ConnectCommand(this.axEntryPoint2Button, CommandKinds.CommandKind_ExecutionEntryPoints_Set, 1, CommandConnectionOptions.CommandConnection_NoOptions);
                    this.axSequenceFileViewMgr.ConnectCommand(this.axRunSequenceButton, CommandKinds.CommandKind_RunCurrentSequence, 0, CommandConnectionOptions.CommandConnection_NoOptions);
                    this.axSequenceFileViewMgr.ConnectCaption(this.axSequenceFileLabel, CaptionSources.CaptionSource_CurrentSequenceFile, false);
                    //this.axSequenceFileViewMgr.ConnectVariables(this.axFileVariables);
                    this.axSequenceFileViewMgr.ConnectInsertionPalette(this.axInsertionPalette);
                    //this.axSequenceFileViewMgr.ConnectSequenceList(this.axSequencesList).SetColumnVisible(SeqListConnectionColumns.SeqListConnectionColumn_Comments, true);

                    // connect controls on the Execution tab
                    this.axExecutionViewMgr.ConnectExecutionView(this.axExecutionSteps, ExecutionViewOptions.ExecutionViewConnection_NoOptions);
                    this.axExecutionViewMgr.ConnectVariables(this.axExecutionVariables);
                    if (!String.Equals(loginname, "Operator"))
                    {
                        this.axExecutionViewMgr.ConnectCallStack(this.axCallStack);
                        this.axExecutionViewMgr.ConnectThreadList(this.axThreads);
                    }
                    this.axExecutionViewMgr.ConnectCommand(this.axBreakResumeBtn, CommandKinds.CommandKind_BreakResume, 0, CommandConnectionOptions.CommandConnection_NoOptions);
                    this.axExecutionViewMgr.ConnectCommand(this.axTerminateRestartBtn, CommandKinds.CommandKind_TerminateRestart, 0, CommandConnectionOptions.CommandConnection_NoOptions);
                    this.axExecutionViewMgr.ConnectCaption(this.axExecutionLabel, CaptionSources.CaptionSource_CurrentExecution, false);

                    // connect controls on the Report tab
                    this.axExecutionViewMgr.ConnectReportView(this.axReportView);

                    // localize strings in top level menu items and controls
                    this.localizer = new Localizer(this.axApplicationMgr.GetEngine(), this.axApplicationMgr);
                    this.localizer.LocalizeForm(this, "TSUI_OI_MAIN_PANEL", true);		// localize .net controls and TestStand UI Controls

                    //Update Title
                    this.UpdateWindowTitle();

                    // decide which tab pages to initially show
                    this.ShowAppropriateTabs();

                    // give initial control focus to the step list control
                    this.axFileSteps.Select();

                    //configure UI based on editor mode
                    this.setEditMode();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(this, exception.Message);

                   this.axApplicationMgr = appMgr;
                }
            }

            private void SequenceForm_Load(object sender, System.EventArgs e)
            {
                
            }

            // if the execution has a report, switch to the report tab either immediately if the execution is visible, or, whenever the execution is viewed next
            private void ShowReport(Execution execution)
            {
                // switch to report view when this execution is next viewed
                execution.AsPropertyObject().SetValNumber("NIUI.LastActiveTab", PropertyOptions.PropOption_InsertIfMissing, (double)TabPageIdentifier.Report); // activate the reportTab when the user views this execution

                if (execution.Id == this.axExecutionViewMgr.Execution.Id) // is this execution the currently displayed execution?			
                    this.ShowAppropriateTabs();  // switch to report view tab now
            }
   
          
            protected override void WndProc(ref Message msg)
            {
                // set the sessionEnding flag so I will know the form is closing because the user
                // is trying to shutdown, restart, or logoff windows
                if (msg.Msg == WM_QUERYENDSESSION)
                {
                    sessionEnding = true;
                    Application.Exit();
                }
                base.WndProc(ref msg);
            }

            // ApplicationMgr sends this message when it's busy doing something so we know to display a hourglass cursor or an equivalent
            private void axApplicationMgr_Wait(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_WaitEvent e)
            {
                if (e.showWait)
                    this.Cursor = Cursors.WaitCursor;
                else
                    this.Cursor = Cursors.Default;
            }

            // the ApplicationMgr sends this event to request that the UI display the report for a particular execution
            private void axApplicationMgr_DisplayReport(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplayReportEvent e)
            {
                this.ShowReport(e.exec);
            }

            // the ApplicationMgr sends this event to request that the UI display a particular execution
            private void axApplicationMgr_DisplayExecution(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplayExecutionEvent e)
            {
                // bring application to front if we hit a breakpoint
                if (e.reason == ExecutionDisplayReasons.ExecutionDisplayReason_Breakpoint || e.reason == ExecutionDisplayReasons.ExecutionDisplayReason_BreakOnRunTimeError)
                    this.Activate();

                // show this execution
                
            }

            // the ApplicationMgr sends this event to report any error that occurs when the TestStand UI controls respond to user input (ie, an error 
            // other than one returned by a method you call directly). For example, if a TestStand menu command generates an error, this handler displays it
            private void axApplicationMgr_ReportError(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_ReportErrorEvent e)
            {
                MessageBox.Show(this, ErrorMessage.AppendCodeAndDescription(this.axApplicationMgr, e.errorMessage, e.errorCode), errorDlgTitle, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // the ApplicationMgr sends this event whenever an execution starts
            private void axApplicationMgr_StartExecution(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_StartExecutionEvent e)
            {
                // add a custom property to the execution to store which tab we are displaying for this execution. Initially show the execution tab
                e.exec.AsPropertyObject().SetValNumber("NIUI.LastActiveTab", PropertyOptions.PropOption_InsertIfMissing, (double)TabPageIdentifier.Execution);
            }

           // build a context menu for a control that has been right-clicked
            private void BuildCommandSetMenu(CommandKinds commandSet, int menuHandle)
            {
                Commands cmds = this.axApplicationMgr.NewCommands();
                int unused;

                // insert items for the specified command or command set in the context menu
                cmds.InsertKind(commandSet, GetActiveViewManager(), -1, "", "", out unused);
                Menus.RemoveInvalidShortcutKeys(cmds);	// remove any shortcuts that .NET does not support
                cmds.InsertIntoWin32Menu(menuHandle, -1, true, true);   // we are using the context menu that the control provides because it requires fewer lines of code. We could have built a .net context menu instead. If you build a .net context menu, you should note that they do not display for ActiveX controls. However, you can display a context menu for the parent form in response to a ActiveX control right-mouse-click event and achieve the same result. To display the context menu at the correct location, convert the click coordinates from control coordinates to form coordinates
            }

            // create a right-click menu for the SequenceView control that displays execution steps
            private void axExecutionView_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._SequenceViewEvents_CreateContextMenuEvent e)
            {
                this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultSequenceViewContextMenu_Set, e.menuHandle);
            }

            // create a right-click menu for the SequenceView control that displays sequence file steps
            private void axSequenceView_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._SequenceViewEvents_CreateContextMenuEvent e)
            {
                this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultSequenceViewContextMenu_Set, e.menuHandle);
            }

            // create a right-click menu for the ListBar control
            private void axListBar_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ListBarEvents_CreateContextMenuEvent e)
            {
                this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultListBarContextMenu_Set, e.menuHandle);
            }

            // create a right-click menu for the sequence list box control
            private void axSequencesList_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ListBoxEvents_CreateContextMenuEvent e)
            {
                this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultSequenceListContextMenu_Set, e.menuHandle);
            }

            // the ListBar sends this event when the listbar switches to a new page
            private void axListBar_CurPageChanged(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ListBarEvents_CurPageChangedEvent e)
            {
                this.ShowAppropriateTabs();
                    // even if the current file and execution haven't changed, what we show in the title bar changes depending on which listbar page is active
            }

            // append the caption for the selected file or execution to the application window title
            private void UpdateWindowTitle()
            {
                object unused;
                string title = this.axApplicationMgr.GetEngine().GetResourceString("TSUI_OI_MAIN_PANEL", "TESTSTAND_USER_INTERFACE", "", out unused);
                string documentDescription = null;

                if (this.tabControl.SelectedIndex == 0)	// sequence files are visible
                    documentDescription += this.axSequenceFileViewMgr.GetCaptionText(CaptionSources.CaptionSource_CurrentSequenceFile, false, "");
                else	// executions are visible
                    documentDescription += this.axExecutionViewMgr.GetCaptionText(CaptionSources.CaptionSource_CurrentExecution, false, "");

                if (documentDescription != null && documentDescription != "")
                    title += " - " + documentDescription;

                this.Text = title;
            }

            // a hidden label control is connected to CaptionSource_CurrentSequenceFile so we can get this event when that caption changes and thus update the title bar in case the title bar is showing the current file
            private void axSequenceFileLabel_ConnectionActivity(object sender, NationalInstruments.TestStand.Interop.UI.Ax._LabelEvents_ConnectionActivityEvent e)
            {
                this.UpdateWindowTitle();
            }

            // a hidden label control is connected to CaptionSource_CurrentExecution so we can get this event when that caption changes and thus update the title bar in case the title bar is showing the current execution
            private void axExecutionLabel_ConnectionActivity(object sender, NationalInstruments.TestStand.Interop.UI.Ax._LabelEvents_ConnectionActivityEvent e)
            {
                this.UpdateWindowTitle();
            }

            // based on the current listbar page, show and hide the tabs that appear in the space to the right of the listbar
            private void ShowAppropriateTabs()
            {
                programmaticallyUpdatingTabPages = true;

                TabPage pageToDisplay = null;

                if (this.tabControl.SelectedIndex == 0)
                    pageToDisplay = this.fileTab;
                else
                {	// we are viewing an execution...
                    pageToDisplay = this.executionTab; // default tab

                    // if the report tab page was the last tab displayed for this execution, re-activate it instead
                    if (this.axExecutionViewMgr.Execution != null)
                        if (TabPageIdentifier.Report == (TabPageIdentifier)this.axExecutionViewMgr.Execution.AsPropertyObject().GetValNumber("NIUI.LastActiveTab", PropertyOptions.PropOption_InsertIfMissing))
                            pageToDisplay = this.reportTab;
                }

                if ((this.tabControl.SelectedTab != pageToDisplay) ||	// prevent flashing if tab page has not changed
                    (this.tabControl.TabCount == 3))					// if all three tabs are visible at once, then we have never called this function before and we need to display the correct tabs
                {
                    // show and hide tabs as appropriate (.NET doesn't let you hide a tab page, so we remove all pages and add back the ones we want to show)
                    this.tabControl.TabPages.Clear();

                    // store the current Active control as tabControl.SelectedTab changes focus to the first control on the tab
                    Control curActiveCtl = this.ActiveControl;

                    if (pageToDisplay == this.fileTab)
                        this.tabControl.TabPages.Add(this.fileTab);
                    else
                    {
                        this.tabControl.TabPages.Add(this.executionTab);
                        this.tabControl.TabPages.Add(this.reportTab);
                    }

                    this.tabControl.SelectedTab = pageToDisplay;

                    // set the current Active control as tabControl.SelectedTab changes focus to the first control on the tab
                    this.ActiveControl = curActiveCtl;
                }

                programmaticallyUpdatingTabPages = false;

            }

            

            // user toggled edit mode. the only way to do that in this application is to type ctrl-shift-alt-insert, which is the edit mode toggle key this application specifies in designer for the ApplicationMgr control. 
            // to prevent edit mode from being toggled with a hotkey, set ApplicationMgr.EditModeShortcutKey to ShortcutKey_VK_NOT_A_KEY


            public object GetActiveViewManager()
            {
                if (this.tabControl.SelectedIndex == 0)	// sequence files are visible, sequence file menu commands apply
                    return this.axSequenceFileViewMgr;
                else if (this.tabControl.SelectedIndex >= 1)	// executions are visible, execution menu commands apply
                    return this.axExecutionViewMgr;
                else
                    return null;
            }

            // return the ExecutionViewMgr or SequenceFileViewMgr for the active MDI child window, or the ApplicationMgr, if no child window
 

            public void DisplayReport()
            {
                this.tabControl.SelectedTab = this.reportTab;
            }

            public void DisplayExecution()
            {
                this.tabControl.SelectedTab = this.executionTab;
            }

            public void DisplaySequenceFile()
            {
                this.tabControl.SelectedTab = this.fileTab;

            }

            public Object GetViewMgr()
            {
                if (tabControl.SelectedTab == fileTab)
                    return this.axSequenceFileViewMgr.GetOcx();
                else
                    return this.axExecutionViewMgr.GetOcx();
            }

            private void SequenceForm_Closing(object sender, FormClosingEventArgs e)
            {
                
                if (this.axExecutionViewMgr.Execution != null)
                {
                    this.axApplicationMgr.CloseExecution(this.axExecutionViewMgr.Execution);
                    e.Cancel = true;
                }
                if (this.axSequenceFileViewMgr.SequenceFile != null)
                {
                    this.axApplicationMgr.CloseSequenceFile(this.axSequenceFileViewMgr.SequenceFile);
                    e.Cancel = true;
                }
                
            }

            public bool isSequenceViewType()
            {
                if (this.axSequenceFileViewMgr.SequenceFile != null)
                {
                    return true;
                }
                if (this.axExecutionViewMgr.Execution != null)
                {
                    return false;
                }
                return false;
            }

            public void setEditMode()
            {
               splitContainer2.Panel1Collapsed = !this.axApplicationMgr.IsEditor;
            }

            //once executionManager has closed the execution, close the window as well
            private void axExecutionViewMgr_ExecutionChanged(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ExecutionViewMgrEvents_ExecutionChangedEvent e)
            {
                if (e.exec == null)
                    this.Close();
            }

            private void axSequenceFileViewMgr_SequenceFileChanged(object sender, NationalInstruments.TestStand.Interop.UI.Ax._SequenceFileViewMgrEvents_SequenceFileChangedEvent e)
            {
                if (e.newFile == null)
                    this.Close();
            }

            private void axExecutionViewMgr_RunStateChanged(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ExecutionViewMgrEvents_RunStateChangedEvent e)
            {
                this.Icon = new System.Drawing.Icon(axApplicationMgr.GetEngine().GetTestStandPath(TestStandPaths.TestStandPath_TestStand) + "\\Components\\Icons\\" + axExecutionViewMgr.GetImageName(ImageSources.ImageSource_ModelState));
            }

        }


}