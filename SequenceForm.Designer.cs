namespace TestExec
{
    partial class SequenceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SequenceForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.fileTab = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.axInsertionPalette = new NationalInstruments.TestStand.Interop.UI.Ax.AxInsertionPalette();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.axFileSteps = new NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceView();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.axSequenceFileViewMgr = new NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceFileViewMgr();
            this.axSequencesList = new NationalInstruments.TestStand.Interop.UI.Ax.AxListBox();
            this.axExecutionViewMgr = new NationalInstruments.TestStand.Interop.UI.Ax.AxExecutionViewMgr();
            this.axFileVariables = new NationalInstruments.TestStand.Interop.UI.Ax.AxVariablesView();
            this.axEntryPoint1Button = new NationalInstruments.TestStand.Interop.UI.Ax.AxButton();
            this.axSequenceFileLabel = new NationalInstruments.TestStand.Interop.UI.Ax.AxLabel();
            this.axEntryPoint2Button = new NationalInstruments.TestStand.Interop.UI.Ax.AxButton();
            this.axRunSequenceButton = new NationalInstruments.TestStand.Interop.UI.Ax.AxButton();
            this.executionTab = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.ExecSplitContainer2 = new System.Windows.Forms.SplitContainer();
            this.axExecutionSteps = new NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceView();
            this.ExecSplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.axExecutionVariables = new NationalInstruments.TestStand.Interop.UI.Ax.AxVariablesView();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.axThreads = new NationalInstruments.TestStand.Interop.UI.Ax.AxListBox();
            this.axCallStack = new NationalInstruments.TestStand.Interop.UI.Ax.AxListBox();
            this.axExecutionLabel = new NationalInstruments.TestStand.Interop.UI.Ax.AxLabel();
            this.axTerminateRestartBtn = new NationalInstruments.TestStand.Interop.UI.Ax.AxButton();
            this.axBreakResumeBtn = new NationalInstruments.TestStand.Interop.UI.Ax.AxButton();
            this.reportTab = new System.Windows.Forms.TabPage();
            this.axReportView = new NationalInstruments.TestStand.Interop.UI.Ax.AxReportView();
            this.tabControl.SuspendLayout();
            this.fileTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axInsertionPalette)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axFileSteps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceFileViewMgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequencesList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionViewMgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axFileVariables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axEntryPoint1Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceFileLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axEntryPoint2Button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axRunSequenceButton)).BeginInit();
            this.executionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExecSplitContainer2)).BeginInit();
            this.ExecSplitContainer2.Panel1.SuspendLayout();
            this.ExecSplitContainer2.Panel2.SuspendLayout();
            this.ExecSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionSteps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExecSplitContainer1)).BeginInit();
            this.ExecSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionVariables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axThreads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axCallStack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axTerminateRestartBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axBreakResumeBtn)).BeginInit();
            this.reportTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axReportView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.fileTab);
            this.tabControl.Controls.Add(this.executionTab);
            this.tabControl.Controls.Add(this.reportTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(972, 629);
            this.tabControl.TabIndex = 2;
            // 
            // fileTab
            // 
            this.fileTab.Controls.Add(this.splitContainer1);
            this.fileTab.Location = new System.Drawing.Point(4, 22);
            this.fileTab.Name = "fileTab";
            this.fileTab.Size = new System.Drawing.Size(964, 603);
            this.fileTab.TabIndex = 0;
            this.fileTab.Text = "FILE_TAB";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.axEntryPoint1Button);
            this.splitContainer1.Panel2.Controls.Add(this.axSequenceFileLabel);
            this.splitContainer1.Panel2.Controls.Add(this.axEntryPoint2Button);
            this.splitContainer1.Panel2.Controls.Add(this.axRunSequenceButton);
            this.splitContainer1.Size = new System.Drawing.Size(964, 603);
            this.splitContainer1.SplitterDistance = 573;
            this.splitContainer1.TabIndex = 10;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.axInsertionPalette);
            this.splitContainer2.Panel1Collapsed = true;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(964, 573);
            this.splitContainer2.SplitterDistance = 204;
            this.splitContainer2.TabIndex = 0;
            // 
            // axInsertionPalette
            // 
            this.axInsertionPalette.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axInsertionPalette.Location = new System.Drawing.Point(0, 0);
            this.axInsertionPalette.Name = "axInsertionPalette";
            this.axInsertionPalette.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axInsertionPalette.OcxState")));
            this.axInsertionPalette.Size = new System.Drawing.Size(204, 100);
            this.axInsertionPalette.TabIndex = 4;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.axFileSteps);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer3.Size = new System.Drawing.Size(964, 573);
            this.splitContainer3.SplitterDistance = 569;
            this.splitContainer3.TabIndex = 0;
            // 
            // axFileSteps
            // 
            this.axFileSteps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axFileSteps.Enabled = true;
            this.axFileSteps.Location = new System.Drawing.Point(0, 0);
            this.axFileSteps.Name = "axFileSteps";
            this.axFileSteps.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axFileSteps.OcxState")));
            this.axFileSteps.Size = new System.Drawing.Size(569, 573);
            this.axFileSteps.TabIndex = 3;
            this.axFileSteps.CreateContextMenu += new NationalInstruments.TestStand.Interop.UI.Ax._SequenceViewEvents_CreateContextMenuEventHandler(this.axSequenceView_CreateContextMenu);
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.axSequenceFileViewMgr);
            this.splitContainer5.Panel1.Controls.Add(this.axSequencesList);
            this.splitContainer5.Panel1.Controls.Add(this.axExecutionViewMgr);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.axFileVariables);
            this.splitContainer5.Size = new System.Drawing.Size(391, 573);
            this.splitContainer5.SplitterDistance = 275;
            this.splitContainer5.TabIndex = 0;
            // 
            // axSequenceFileViewMgr
            // 
            this.axSequenceFileViewMgr.Enabled = true;
            this.axSequenceFileViewMgr.Location = new System.Drawing.Point(167, 3);
            this.axSequenceFileViewMgr.Name = "axSequenceFileViewMgr";
            this.axSequenceFileViewMgr.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSequenceFileViewMgr.OcxState")));
            this.axSequenceFileViewMgr.Size = new System.Drawing.Size(32, 32);
            this.axSequenceFileViewMgr.TabIndex = 24;
            this.axSequenceFileViewMgr.SequenceFileChanged += new NationalInstruments.TestStand.Interop.UI.Ax._SequenceFileViewMgrEvents_SequenceFileChangedEventHandler(this.axSequenceFileViewMgr_SequenceFileChanged);
            // 
            // axSequencesList
            // 
            this.axSequencesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axSequencesList.Location = new System.Drawing.Point(0, 0);
            this.axSequencesList.Name = "axSequencesList";
            this.axSequencesList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSequencesList.OcxState")));
            this.axSequencesList.Size = new System.Drawing.Size(391, 275);
            this.axSequencesList.TabIndex = 3;
            // 
            // axExecutionViewMgr
            // 
            this.axExecutionViewMgr.Enabled = true;
            this.axExecutionViewMgr.Location = new System.Drawing.Point(179, 0);
            this.axExecutionViewMgr.Name = "axExecutionViewMgr";
            this.axExecutionViewMgr.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axExecutionViewMgr.OcxState")));
            this.axExecutionViewMgr.Size = new System.Drawing.Size(32, 32);
            this.axExecutionViewMgr.TabIndex = 23;
            this.axExecutionViewMgr.ExecutionChanged += new NationalInstruments.TestStand.Interop.UI.Ax._ExecutionViewMgrEvents_ExecutionChangedEventHandler(this.axExecutionViewMgr_ExecutionChanged);
            this.axExecutionViewMgr.RunStateChanged += new NationalInstruments.TestStand.Interop.UI.Ax._ExecutionViewMgrEvents_RunStateChangedEventHandler(this.axExecutionViewMgr_RunStateChanged);
            // 
            // axFileVariables
            // 
            this.axFileVariables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axFileVariables.Location = new System.Drawing.Point(0, 0);
            this.axFileVariables.Name = "axFileVariables";
            this.axFileVariables.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axFileVariables.OcxState")));
            this.axFileVariables.Size = new System.Drawing.Size(391, 294);
            this.axFileVariables.TabIndex = 2;
            // 
            // axEntryPoint1Button
            // 
            this.axEntryPoint1Button.Location = new System.Drawing.Point(0, 1);
            this.axEntryPoint1Button.Name = "axEntryPoint1Button";
            this.axEntryPoint1Button.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axEntryPoint1Button.OcxState")));
            this.axEntryPoint1Button.Size = new System.Drawing.Size(152, 24);
            this.axEntryPoint1Button.TabIndex = 4;
            // 
            // axSequenceFileLabel
            // 
            this.axSequenceFileLabel.Location = new System.Drawing.Point(462, 6);
            this.axSequenceFileLabel.Name = "axSequenceFileLabel";
            this.axSequenceFileLabel.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSequenceFileLabel.OcxState")));
            this.axSequenceFileLabel.Size = new System.Drawing.Size(352, 13);
            this.axSequenceFileLabel.TabIndex = 9;
            this.axSequenceFileLabel.TabStop = false;
            this.axSequenceFileLabel.Visible = false;
            this.axSequenceFileLabel.ConnectionActivity += new NationalInstruments.TestStand.Interop.UI.Ax._LabelEvents_ConnectionActivityEventHandler(this.axSequenceFileLabel_ConnectionActivity);
            // 
            // axEntryPoint2Button
            // 
            this.axEntryPoint2Button.Location = new System.Drawing.Point(152, 1);
            this.axEntryPoint2Button.Name = "axEntryPoint2Button";
            this.axEntryPoint2Button.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axEntryPoint2Button.OcxState")));
            this.axEntryPoint2Button.Size = new System.Drawing.Size(152, 24);
            this.axEntryPoint2Button.TabIndex = 5;
            // 
            // axRunSequenceButton
            // 
            this.axRunSequenceButton.Location = new System.Drawing.Point(304, 1);
            this.axRunSequenceButton.Name = "axRunSequenceButton";
            this.axRunSequenceButton.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axRunSequenceButton.OcxState")));
            this.axRunSequenceButton.Size = new System.Drawing.Size(152, 24);
            this.axRunSequenceButton.TabIndex = 6;
            // 
            // executionTab
            // 
            this.executionTab.Controls.Add(this.splitContainer4);
            this.executionTab.Location = new System.Drawing.Point(4, 22);
            this.executionTab.Name = "executionTab";
            this.executionTab.Size = new System.Drawing.Size(964, 603);
            this.executionTab.TabIndex = 1;
            this.executionTab.Text = "EXECUTION_TAB";
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.ExecSplitContainer2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.axTerminateRestartBtn);
            this.splitContainer4.Panel2.Controls.Add(this.axBreakResumeBtn);
            this.splitContainer4.Size = new System.Drawing.Size(964, 603);
            this.splitContainer4.SplitterDistance = 572;
            this.splitContainer4.TabIndex = 9;
            // 
            // ExecSplitContainer2
            // 
            this.ExecSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExecSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.ExecSplitContainer2.Name = "ExecSplitContainer2";
            this.ExecSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ExecSplitContainer2.Panel1
            // 
            this.ExecSplitContainer2.Panel1.Controls.Add(this.axExecutionSteps);
            this.ExecSplitContainer2.Panel1.Controls.Add(this.ExecSplitContainer1);
            // 
            // ExecSplitContainer2.Panel2
            // 
            this.ExecSplitContainer2.Panel2.Controls.Add(this.splitContainer7);
            this.ExecSplitContainer2.Panel2.Controls.Add(this.axExecutionLabel);
            this.ExecSplitContainer2.Size = new System.Drawing.Size(964, 572);
            this.ExecSplitContainer2.SplitterDistance = 299;
            this.ExecSplitContainer2.TabIndex = 8;
            // 
            // axExecutionSteps
            // 
            this.axExecutionSteps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axExecutionSteps.Enabled = true;
            this.axExecutionSteps.Location = new System.Drawing.Point(0, 0);
            this.axExecutionSteps.Name = "axExecutionSteps";
            this.axExecutionSteps.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axExecutionSteps.OcxState")));
            this.axExecutionSteps.Size = new System.Drawing.Size(964, 299);
            this.axExecutionSteps.TabIndex = 0;
            this.axExecutionSteps.CreateContextMenu += new NationalInstruments.TestStand.Interop.UI.Ax._SequenceViewEvents_CreateContextMenuEventHandler(this.axExecutionView_CreateContextMenu);
            // 
            // ExecSplitContainer1
            // 
            this.ExecSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExecSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.ExecSplitContainer1.Name = "ExecSplitContainer1";
            this.ExecSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.ExecSplitContainer1.Size = new System.Drawing.Size(964, 299);
            this.ExecSplitContainer1.SplitterDistance = 146;
            this.ExecSplitContainer1.TabIndex = 7;
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.axExecutionVariables);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer7.Size = new System.Drawing.Size(964, 269);
            this.splitContainer7.SplitterDistance = 506;
            this.splitContainer7.TabIndex = 7;
            // 
            // axExecutionVariables
            // 
            this.axExecutionVariables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axExecutionVariables.Location = new System.Drawing.Point(0, 0);
            this.axExecutionVariables.Name = "axExecutionVariables";
            this.axExecutionVariables.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axExecutionVariables.OcxState")));
            this.axExecutionVariables.Size = new System.Drawing.Size(506, 269);
            this.axExecutionVariables.TabIndex = 1;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.axThreads);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.axCallStack);
            this.splitContainer6.Size = new System.Drawing.Size(454, 269);
            this.splitContainer6.SplitterDistance = 228;
            this.splitContainer6.TabIndex = 3;
            // 
            // axThreads
            // 
            this.axThreads.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axThreads.Location = new System.Drawing.Point(0, 0);
            this.axThreads.Name = "axThreads";
            this.axThreads.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axThreads.OcxState")));
            this.axThreads.Size = new System.Drawing.Size(228, 269);
            this.axThreads.TabIndex = 3;
            this.axThreads.CreateContextMenu += new NationalInstruments.TestStand.Interop.UI.Ax._ListBoxEvents_CreateContextMenuEventHandler(this.axSequencesList_CreateContextMenu);
            // 
            // axCallStack
            // 
            this.axCallStack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axCallStack.Location = new System.Drawing.Point(0, 0);
            this.axCallStack.Name = "axCallStack";
            this.axCallStack.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCallStack.OcxState")));
            this.axCallStack.Size = new System.Drawing.Size(222, 269);
            this.axCallStack.TabIndex = 2;
            this.axCallStack.CreateContextMenu += new NationalInstruments.TestStand.Interop.UI.Ax._ListBoxEvents_CreateContextMenuEventHandler(this.axSequencesList_CreateContextMenu);
            // 
            // axExecutionLabel
            // 
            this.axExecutionLabel.Location = new System.Drawing.Point(9, 2);
            this.axExecutionLabel.Name = "axExecutionLabel";
            this.axExecutionLabel.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axExecutionLabel.OcxState")));
            this.axExecutionLabel.Size = new System.Drawing.Size(331, 13);
            this.axExecutionLabel.TabIndex = 6;
            this.axExecutionLabel.TabStop = false;
            this.axExecutionLabel.Visible = false;
            this.axExecutionLabel.ConnectionActivity += new NationalInstruments.TestStand.Interop.UI.Ax._LabelEvents_ConnectionActivityEventHandler(this.axExecutionLabel_ConnectionActivity);
            // 
            // axTerminateRestartBtn
            // 
            this.axTerminateRestartBtn.Location = new System.Drawing.Point(129, 1);
            this.axTerminateRestartBtn.Name = "axTerminateRestartBtn";
            this.axTerminateRestartBtn.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axTerminateRestartBtn.OcxState")));
            this.axTerminateRestartBtn.Size = new System.Drawing.Size(127, 24);
            this.axTerminateRestartBtn.TabIndex = 7;
            // 
            // axBreakResumeBtn
            // 
            this.axBreakResumeBtn.Location = new System.Drawing.Point(0, 1);
            this.axBreakResumeBtn.Name = "axBreakResumeBtn";
            this.axBreakResumeBtn.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axBreakResumeBtn.OcxState")));
            this.axBreakResumeBtn.Size = new System.Drawing.Size(127, 24);
            this.axBreakResumeBtn.TabIndex = 4;
            // 
            // reportTab
            // 
            this.reportTab.Controls.Add(this.axReportView);
            this.reportTab.Location = new System.Drawing.Point(4, 22);
            this.reportTab.Name = "reportTab";
            this.reportTab.Size = new System.Drawing.Size(964, 603);
            this.reportTab.TabIndex = 2;
            this.reportTab.Text = "REPORT_TAB";
            // 
            // axReportView
            // 
            this.axReportView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axReportView.Enabled = true;
            this.axReportView.Location = new System.Drawing.Point(0, 0);
            this.axReportView.Name = "axReportView";
            this.axReportView.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axReportView.OcxState")));
            this.axReportView.Size = new System.Drawing.Size(964, 603);
            this.axReportView.TabIndex = 0;
            // 
            // SequenceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 629);
            this.Controls.Add(this.tabControl);
            this.Name = "SequenceForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SequenceForm_Closing);
            this.tabControl.ResumeLayout(false);
            this.fileTab.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axInsertionPalette)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axFileSteps)).EndInit();
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceFileViewMgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequencesList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionViewMgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axFileVariables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axEntryPoint1Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceFileLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axEntryPoint2Button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axRunSequenceButton)).EndInit();
            this.executionTab.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ExecSplitContainer2.Panel1.ResumeLayout(false);
            this.ExecSplitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExecSplitContainer2)).EndInit();
            this.ExecSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionSteps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExecSplitContainer1)).EndInit();
            this.ExecSplitContainer1.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionVariables)).EndInit();
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axThreads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axCallStack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axTerminateRestartBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axBreakResumeBtn)).EndInit();
            this.reportTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axReportView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage fileTab;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxLabel axSequenceFileLabel;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxButton axRunSequenceButton;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxButton axEntryPoint2Button;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxButton axEntryPoint1Button;
        private System.Windows.Forms.TabPage executionTab;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxVariablesView axExecutionVariables;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxLabel axExecutionLabel;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxListBox axCallStack;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceView axExecutionSteps;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxButton axBreakResumeBtn;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxListBox axThreads;
        private System.Windows.Forms.TabPage reportTab;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxReportView axReportView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxListBox axSequencesList;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxVariablesView axFileVariables;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxInsertionPalette axInsertionPalette;
        private System.Windows.Forms.SplitContainer ExecSplitContainer1;
        private System.Windows.Forms.SplitContainer ExecSplitContainer2;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceFileViewMgr axSequenceFileViewMgr;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxExecutionViewMgr axExecutionViewMgr;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxButton axTerminateRestartBtn;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceView axFileSteps;
    }
}