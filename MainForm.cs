// Note:	This application has a manifest file in the project. This manifest file includes the Microsoft.Windows.Common-Controls which 
//			enables the application to display controls using the XP theme that the operating system selects.
//			A post build event embeds this manifest file into the executable.
//			In order for the manifest file to enable the executable to display with the XP theme:
//			1. The manifest file must have the same name as the executable. For example, if your executable is named MyExecutable.exe, your manifest file is required to have the name MyExecutable.exe.manifest.
//			2. The manifest file must include the Microsoft.Windows.Common-Controls.
//			3. The manifest file must reside in the same directory as the executable.
//			Also note that if you enable the Project Properties>>Debug>>Enable Visual Studio Hosting Process option, the XP theme adaption does not occur when debugging the executable
//			because the Visual Studio environment creates the process and does not allow the manifest file to be embedded into the executable.

// Note:	This example can function as an editor or an operator interface. The user can change the edit mode with a keystroke (ctrl-alt-shift-insert) or with a command line 
//			argument. For more information and for instructions on how prevent the user from changing the edit mode, refer to the TestStand Reference Manual>>Creating Custom 
//			User Interfaces>>Editor versus Operator Interface Applications>>Creating Editor Applications.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

// TestStand Core API 
using NationalInstruments.TestStand.Interop.API; 

// TestStand User Interface Controls
using NationalInstruments.TestStand.Interop.UI;
using NationalInstruments.TestStand.Interop.UI.Support;

// .net specific functions for use with TestStand APIs (TSUtil)
using NationalInstruments.TestStand.Utility;

namespace TestExec
{
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr axApplicationMgr;
        private System.Windows.Forms.Timer GCTimer;
        private System.Windows.Forms.MainMenu mainMenu1;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxStatusBar axStatusBar;
		private System.ComponentModel.IContainer components;

		private System.Windows.Forms.MenuItem fileMenu;
		private System.Windows.Forms.MenuItem executeMenu;
		private System.Windows.Forms.MenuItem debugMenu;
		private System.Windows.Forms.MenuItem configureMenu;
		private System.Windows.Forms.MenuItem toolsMenu;
		private System.Windows.Forms.MenuItem helpMenu;
		private System.Windows.Forms.MenuItem aboutBoxItem;

		// Values that identifies a tag page in this application. Because these values can be converted to numbers, we can store them
		// in a TestStand property. We use this to attach a new numeric property to each TestStand execution object to store which tab page 
		// the application displays for that execution.
		enum TabPageIdentifier 
		{
			NotATab,
			SequenceFile,
			Execution,
			Report
		};

        private Point mNewChildFormInitialLocation;
        private const int NewChildFormOffsetX = 30;
        private const int NewChildFormOffsetY = 30;

		// list bar page indices
		private const int	SEQUENCE_FILES_PAGE_INDEX = 0;	// first page in list bar
		private const int	EXECUTIONS_PAGE_INDEX = 1;		// second page in list bar

		// global flag that indicates if we are programmatically changing the active tab page
		private bool		programmaticallyUpdatingTabPages = false;

		private const int	WM_QUERYENDSESSION = 0x11;

		// flag that will be set to true if the user tries to shut down windows
		private bool		sessionEnding = false;

		// a localizer converts text in .NET menus and Forms to the current language by obtaining the translations from the TestStand string resource files
		private Localizer	localizer;
        private Hashtable mChildFormsKeyedByViewMgr;		// maps viewMgrs to the forms on which they reside
        private System.Windows.Forms.MenuItem editMenu;
        private MenuItem menuItem1;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
        private MenuItem menuItem4;
        private MenuItem menuItem5;
        private MenuItem menuItem6;
        private string errorDlgTitle = "Error"; // localized in MainForm_Load

		public MainForm()
		{
			// Required for Windows Form Designer support
			InitializeComponent();

            this.mChildFormsKeyedByViewMgr = new Hashtable();
			Icon = Properties.Resources.testexec;
            
            
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		 #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.GCTimer = new System.Windows.Forms.Timer(this.components);
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenu = new System.Windows.Forms.MenuItem();
            this.editMenu = new System.Windows.Forms.MenuItem();
            this.executeMenu = new System.Windows.Forms.MenuItem();
            this.debugMenu = new System.Windows.Forms.MenuItem();
            this.configureMenu = new System.Windows.Forms.MenuItem();
            this.toolsMenu = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.helpMenu = new System.Windows.Forms.MenuItem();
            this.aboutBoxItem = new System.Windows.Forms.MenuItem();
            this.axStatusBar = new NationalInstruments.TestStand.Interop.UI.Ax.AxStatusBar();
            this.axApplicationMgr = new NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr();
            ((System.ComponentModel.ISupportInitialize)(this.axStatusBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axApplicationMgr)).BeginInit();
            this.SuspendLayout();
            // 
            // GCTimer
            // 
            this.GCTimer.Interval = 3000;
            this.GCTimer.Tick += new System.EventHandler(this.GCTimerTick);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenu,
            this.editMenu,
            this.executeMenu,
            this.debugMenu,
            this.configureMenu,
            this.toolsMenu,
            this.menuItem1,
            this.helpMenu});
            // 
            // fileMenu
            // 
            this.fileMenu.Index = 0;
            this.fileMenu.Text = "FILE";
            // 
            // editMenu
            // 
            this.editMenu.Index = 1;
            this.editMenu.Text = "EDIT";
            // 
            // executeMenu
            // 
            this.executeMenu.Index = 2;
            this.executeMenu.Text = "EXECUTE";
            // 
            // debugMenu
            // 
            this.debugMenu.Index = 3;
            this.debugMenu.Text = "DEBUG";
            // 
            // configureMenu
            // 
            this.configureMenu.Index = 4;
            this.configureMenu.Text = "CONFIGURE";
            // 
            // toolsMenu
            // 
            this.toolsMenu.Index = 5;
            this.toolsMenu.Text = "TOOLS";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 6;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem3});
            this.menuItem1.Text = "WINDOW";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "CASCADE";
            this.menuItem2.Click += new System.EventHandler(this.cascadeMenuItem_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem4,
            this.menuItem5,
            this.menuItem6});
            this.menuItem3.Text = "TILE";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 0;
            this.menuItem4.Text = "TILE_EVENLY";
            this.menuItem4.Click += new System.EventHandler(this.tileMenuItem_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.Text = "TILE_HORIZONTAL";
            this.menuItem5.Click += new System.EventHandler(this.tileHorizontalMenuItem_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 2;
            this.menuItem6.Text = "TILE_VERTICAL";
            this.menuItem6.Click += new System.EventHandler(this.tileVerticalMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.Index = 7;
            this.helpMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.aboutBoxItem});
            this.helpMenu.Text = "HELP";
            // 
            // aboutBoxItem
            // 
            this.aboutBoxItem.Index = 0;
            this.aboutBoxItem.Text = "ABOUT";
            this.aboutBoxItem.Click += new System.EventHandler(this.aboutBoxItem_Click);
            // 
            // axStatusBar
            // 
            this.axStatusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.axStatusBar.Enabled = true;
            this.axStatusBar.Location = new System.Drawing.Point(0, 474);
            this.axStatusBar.Name = "axStatusBar";
            this.axStatusBar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axStatusBar.OcxState")));
            this.axStatusBar.Size = new System.Drawing.Size(928, 20);
            this.axStatusBar.TabIndex = 19;
            this.axStatusBar.TabStop = false;
            // 
            // axApplicationMgr
            // 
            this.axApplicationMgr.Enabled = true;
            this.axApplicationMgr.Location = new System.Drawing.Point(884, 12);
            this.axApplicationMgr.Name = "axApplicationMgr";
            this.axApplicationMgr.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axApplicationMgr.OcxState")));
            this.axApplicationMgr.Size = new System.Drawing.Size(32, 32);
            this.axApplicationMgr.TabIndex = 20;
            this.axApplicationMgr.ExitApplication += new System.EventHandler(this.axApplicationMgr_ExitApplication);
            this.axApplicationMgr.UIMessageEvent += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_UIMessageEventEventHandler(this.axApplicationMgr_UIMessageEvent);
            this.axApplicationMgr.Wait += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_WaitEventHandler(this.axApplicationMgr_Wait);
            this.axApplicationMgr.ReportError += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_ReportErrorEventHandler(this.axApplicationMgr_ReportError);
            this.axApplicationMgr.QueryShutdown += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_QueryShutdownEventHandler(this.axApplicationMgr_QueryShutdown);
            this.axApplicationMgr.DisplaySequenceFile += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplaySequenceFileEventHandler(this.axApplicationMgr_DisplaySequenceFile);
            this.axApplicationMgr.DisplayExecution += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplayExecutionEventHandler(this.axApplicationMgr_DisplayExecution);
            this.axApplicationMgr.DisplayReport += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplayReportEventHandler(this.axApplicationMgr_DisplayReport);
            this.axApplicationMgr.StartExecution += new NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_StartExecutionEventHandler(this.axApplicationMgr_StartExecution);
            this.axApplicationMgr.EditModeChanged += new System.EventHandler(this.axApplicationMgr_EditModeChanged);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(928, 494);
            this.Controls.Add(this.axStatusBar);
            this.Controls.Add(this.axApplicationMgr);
            this.IsMdiContainer = true;
            this.Menu = this.mainMenu1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enlighted Test GUI";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MdiChildActivate += new System.EventHandler(this.MainForm_MdiChildActivate);
            this.MenuStart += new System.EventHandler(this.MainForm_MenuStart);
            ((System.ComponentModel.ISupportInitialize)(this.axStatusBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axApplicationMgr)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) 
		{
            //LaunchTestStandApplicationInNewDomain.Launch(new LaunchTestStandApplicationInNewDomain.MainEntryPointDelegateWithArgs(MainEntryPoint), args);
            LaunchTestStandApplicationInNewDomain.LaunchProtected(
                 new LaunchTestStandApplicationInNewDomain.MainEntryPointDelegateWithArgs(MainEntryPoint),
                 args,
                 "TestStand C# UI",
                 new LaunchTestStandApplicationInNewDomain.DisplayErrorMessageDelegate(DisplayError),
                 true);

        }

		static private void MainEntryPoint(string[] args)
		{
			NationalInstruments.TestStand.Utility.ApplicationWrapper.Run(new MainForm());
		}

        static private void DisplayError(string caption, string message)
        {
            MessageBox.Show(message, caption);
        }
        private void MainForm_Load(object sender, System.EventArgs e)
		{
			SplashScreen	splashScreen = null;
            
       
			try
			{
				if (!this.axApplicationMgr.ApplicationWillExitOnStart)
					splashScreen = new SplashScreen();  // show splash screen

				// If this UI is running in a CLR other than the one TestStand uses,
				// then it needs its own GCTimer for that version of the CLR. If it's running in the
				// same CLR as TestStand then the engine's gctimer enabled by the ApplicationMgr
				// is sufficient. See the API help for Engine.DotNetGarbageCollectionInterval for more details.
				//if (System.Environment.Version.ToString() != this.axApplicationMgr.GetEngine().DotNetCLRVersion)
					this.GCTimer.Enabled = true;

				// localize error dialog title bar
				object found;
				errorDlgTitle = this.axApplicationMgr.GetEngine().GetResourceString("TSUI_OI_MAIN_PANEL", "ERR_BOX_TITLE", errorDlgTitle, out found);

				// this application allows setting of breakpoints on sequences files, so let them persist
				this.axApplicationMgr.GetEngine().PersistBreakpoints = true;

                // connect main window status bar panes
                StatusBarPanes panes = this.axStatusBar.Panes;

                // User status bar pane 
                this.axApplicationMgr.ConnectCaption(panes["User"], CaptionSources.CaptionSource_UserName, false);

                // Process Model status bar pane 
                this.axApplicationMgr.ConnectCaption(panes["ProcessModel"], CaptionSources.CaptionSource_CurrentProcessModelFile, false);

                // start up the TestStand User Interface Components. this also logs in the user
				this.axApplicationMgr.Start();

				// build the menubar for the first time (must call initially to ensure MenuStart event is sent for shortcut keys to TestStand command items)
                this.RebuildMenuBar();	
				
				// localize strings in top level menu items and controls
				this.localizer = new Localizer(this.axApplicationMgr.GetEngine(), this.axApplicationMgr);
				this.localizer.LocalizeForm(this, "TSUI_OI_MAIN_PANEL", true);		// localize .net controls and TestStand UI Controls

			}
			catch (Exception exception) 
			{
				MessageBox.Show(this, exception.Message);

				if (splashScreen != null)
					splashScreen.Hide();

				Application.Exit();
			}

			if (splashScreen != null)
				splashScreen.Hide();
		}

		// if the execution has a report, switch to the report tab either immediately if the execution is visible, or, whenever the execution is viewed next

		// show the sequence file list and execution list in list bar pages

        
		private void ConnectStatusBarPanes()
		{
			StatusBarPanes	panes = this.axStatusBar.Panes;

			// User
			this.axApplicationMgr.ConnectCaption(panes["User"], CaptionSources.CaptionSource_UserName, false);
			
		}
      
		// handle request to close form (via Windows close box, for example)
		private void MainForm_Closing(object sender, FormClosingEventArgs e)
		{
			// Don't set e.Cancel to true if windows is shutting down.
			// Doing so would prevent windows from shutting down or logging out.
			if (!sessionEnding)
			{
				// initiate shutdown and cancel close if shutdown is not complete.  The applicationMgr will
				// send the ExitApplication event when shutdown is complete and we can close then
				if (axApplicationMgr.Shutdown() == false)
					e.Cancel = true;
				else
				{
					this.localizer = null;
				}
			}
		}



		// It is now ok to exit, close the form
		private void axApplicationMgr_ExitApplication(object sender, System.EventArgs e)
		{
			// discard any current menu items that were inserted by Menus.InsertCommandsInMenu. These menu items might refer to TestStand objects, so delete them before the engine is destroyed. Note that it is too early to do this in QueryShutdown because a menu might be used after QueryShutdown returns, particularly if an unload callback runs.
			Menus.RemoveMenuCommands(this.mainMenu1, false, false);
			//Environment.ExitCode = this.axApplicationMgr.ExitCode;
			this.Close();

			//TSHelper.DoSynchronousGCForCOMObjectDestruction();
		}

		// release any TestStand objects and save any settings here
		private void axApplicationMgr_QueryShutdown(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_QueryShutdownEvent e)
		{
			//LayoutPersister.SaveSizes(this.axApplicationMgr, this.axListBar, this.tabControl, this.axFileSteps, this.axSequencesList, this.axFileVariables, this.axInsertionPalette, this.axExecutionSteps, this.axCallStack, this.axThreads, this.axExecutionVariables);
			//LayoutPersister.SaveBounds(this.axApplicationMgr, this);
		}

		// ApplicationMgr sends this message when it's busy doing something so we know to display a hourglass cursor or an equivalent
		private void axApplicationMgr_Wait(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_WaitEvent e)
		{
			if (e.showWait)
				this.Cursor = Cursors.WaitCursor;
			else
				this.Cursor = Cursors.Default;
		}
		
		// the ApplicationMgr sends this event to request that the UI display the report for a particular execution
		private void axApplicationMgr_DisplayReport(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplayReportEvent e)
		{
            findFormByExecution(e.exec).DisplayReport();
		}	

		// the ApplicationMgr sends this event to request that the UI display a particular execution
		private void axApplicationMgr_DisplayExecution(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplayExecutionEvent e)
		{
			// bring application to front if we hit a breakpoint
			if (e.reason == ExecutionDisplayReasons.ExecutionDisplayReason_Breakpoint || e.reason == ExecutionDisplayReasons.ExecutionDisplayReason_BreakOnRunTimeError)
				this.Activate();

            DisplayExecution(e.exec);
		}

       

		// the ApplicationMgr sends this event to request that the UI display a particular sequence file
		private void axApplicationMgr_DisplaySequenceFile(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_DisplaySequenceFileEvent e)
		{
            DisplaySequenceFile(e.file);
		}

		// the ApplicationMgr sends this event to report any error that occurs when the TestStand UI controls respond to user input (ie, an error 
		// other than one returned by a method you call directly). For example, if a TestStand menu command generates an error, this handler displays it
		private void axApplicationMgr_ReportError(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_ReportErrorEvent e)
		{
			MessageBox.Show(this, ErrorMessage.AppendCodeAndDescription(this.axApplicationMgr, e.errorMessage, e.errorCode), errorDlgTitle, MessageBoxButtons.OK, MessageBoxIcon.Stop); 		
		}

		// the ApplicationMgr sends this event whenever an execution starts
		private void axApplicationMgr_StartExecution(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_StartExecutionEvent e)
		{
			// add a custom property to the execution to store which tab we are displaying for this execution. Initially show the execution tab
			e.exec.AsPropertyObject().SetValNumber("NIUI.LastActiveTab", PropertyOptions.PropOption_InsertIfMissing, (double)TabPageIdentifier.Execution);
		}

		// build a context menu for a control that has been right-clicked
		private void BuildCommandSetMenu(CommandKinds commandSet, int menuHandle)
		{
			Commands	cmds = this.axApplicationMgr.NewCommands();
			int			unused;

			// insert items for the specified command or command set in the context menu
			cmds.InsertKind(commandSet, GetActiveMgr(), -1, "", "", out unused);
			Menus.RemoveInvalidShortcutKeys(cmds);	// remove any shortcuts that .NET does not support
			cmds.InsertIntoWin32Menu(menuHandle, -1, true, true);   // we are using the context menu that the control provides because it requires fewer lines of code. We could have built a .net context menu instead. If you build a .net context menu, you should note that they do not display for ActiveX controls. However, you can display a context menu for the parent form in response to a ActiveX control right-mouse-click event and achieve the same result. To display the context menu at the correct location, convert the click coordinates from control coordinates to form coordinates
		}

		// create a right-click menu for the SequenceView control that displays execution steps
		private void axExecutionView_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._SequenceViewEvents_CreateContextMenuEvent e)
		{
            this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultSequenceViewContextMenu_Set, e.menuHandle);
		}

		// create a right-click menu for the SequenceView control that displays sequence file steps
		private void axSequenceView_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._SequenceViewEvents_CreateContextMenuEvent e)
		{
            this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultSequenceViewContextMenu_Set, e.menuHandle);
		}

		// create a right-click menu for the ListBar control
		private void axListBar_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ListBarEvents_CreateContextMenuEvent e)
		{
            this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultListBarContextMenu_Set, e.menuHandle);
		}

		// create a right-click menu for the sequence list box control
		private void axSequencesList_CreateContextMenu(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ListBoxEvents_CreateContextMenuEvent e)
		{
            this.BuildCommandSetMenu(CommandKinds.CommandKind_DefaultSequenceListContextMenu_Set, e.menuHandle);
		}

 		// The menu has been accessed, rebuild it with currently applicable items
		private void MainForm_MenuStart(object sender, System.EventArgs e)
		{
            this.RebuildMenuBar();
		}

		// make sure all menus have appropriate items with the correct enabled states
        private void RebuildMenuBar()
        {

            Menus.BeginUpdate(this);

            try
            {
                // discard any current menu items that were inserted by Menus.InsertCommandsInMenu
                Menus.RemoveMenuCommands(this.mainMenu1, false, false);

                object mgr = GetActiveMgr();

                // rebuild File menu
                ArrayList fileMenuCommands = new ArrayList();
                fileMenuCommands.Add(CommandKinds.CommandKind_DefaultFileMenu_Set);				// add all the usual commands in a File menu
                Menus.InsertCommandsInMenu(fileMenuCommands, this.fileMenu, null, mgr, true);
                

                // rebuild Edit menu
                ArrayList editMenuCommands = new ArrayList();
                editMenuCommands.Add(CommandKinds.CommandKind_DefaultEditMenu_Set);				// add all the usual commands in an Edit menu
                Menus.InsertCommandsInMenu(editMenuCommands, this.editMenu, null, mgr, true);
                
                // rebuild Execute menu
                ArrayList executeMenuCommands = new ArrayList();
                executeMenuCommands.Add(CommandKinds.CommandKind_DefaultExecuteMenu_Set);		// add all the usual commands in a Execute menu				
                Menus.InsertCommandsInMenu(executeMenuCommands, this.executeMenu, null, mgr, true);

                // rebuild Debug menu
                ArrayList debugMenuCommands = new ArrayList();
                debugMenuCommands.Add(CommandKinds.CommandKind_DefaultDebugMenu_Set);			// add all the usual commands in a Debug menu
                Menus.InsertCommandsInMenu(debugMenuCommands, this.debugMenu, null, mgr, true);

                // rebuild Configure menu
                ArrayList configureMenuCommands = new ArrayList();
                configureMenuCommands.Add(CommandKinds.CommandKind_DefaultConfigureMenu_Set);	// add all the usual commands in a Configure menu
                Menus.InsertCommandsInMenu(configureMenuCommands, this.configureMenu, null, mgr, true);

                // rebuild Tools menu
                ArrayList toolsMenuCommands = new ArrayList();
                toolsMenuCommands.Add(CommandKinds.CommandKind_DefaultToolsMenu_Set);			// add all the usual commands in a Tools menu
                Menus.InsertCommandsInMenu(toolsMenuCommands, this.toolsMenu, null, mgr, true);

                // remove duplicate separators and shortcut keys
                Menus.CleanupMenu(this.mainMenu1);
            }
            catch (Exception theException)
            {
                MessageBox.Show(theException.Message, "Error");
            }
            this.ResumeLayout();
            Menus.EndUpdate();
        }			

		// Release all objects periodically.  .NET lets COM objects pile up on the managed heap, seemingly even objects you don't know about such
		// as parameters to unhandled ActiveX events.  This timer ensures that all COM objects are released in a timely manner,
		// thus preventing the performance hiccup that could occur when .NET finally decides to collect garbage. Also, this timer
		// ensures that actions triggered by object destruction run in a timely manner. For example: sequence file unload callbacks.
		private void GCTimerTick(object sender, System.EventArgs e)
		{
			GC.Collect(); // force .net garbage collection		
		}				

		// displays the about box when the user selects Help>>About...
		private void aboutBoxItem_Click(object sender, System.EventArgs e)
		{
			using (AboutBox aboutBox = new AboutBox(this.localizer))
			{
				aboutBox.ShowDialog(this);
			}
		}

		// adjust controls to fit within current window size
		

        //ADDED FUNCTIONS


        // display an execution, creating a new window if the execution is not yet in a window
        private void DisplayExecution(Execution execution)
        {
            SequenceForm sequenceForm = (SequenceForm)FindForm(execution);
            FormWindowState newWindowState = this.ActiveMdiChild == null ? FormWindowState.Normal : this.ActiveMdiChild.WindowState; // must get this value before creating a new form (see end of note 2 at top of file)

            if (sequenceForm == null) // execution is not in a form, make a new form
            {
                sequenceForm = new SequenceForm(this, this.axApplicationMgr, execution);
                this.mChildFormsKeyedByViewMgr.Add(sequenceForm.GetViewMgr(), sequenceForm); // use GetOcx to return the activeX control instead of the wrapper because that is what axApplicationMgr.GetExecutionViewMgr returns, and we need to compare apples to apples
                SetNewChildFormLocation(sequenceForm);
            }

            sequenceForm.DisplayExecution(); // switch to the execution tab
            sequenceForm.Show();
            sequenceForm.WindowState = newWindowState; // see end of Note 2 at top of file
            sequenceForm.Activate();
        }

        private void DisplaySequenceFile(SequenceFile file)
        {
            SequenceForm sequenceForm = (SequenceForm)FindForm(file);
            FormWindowState newWindowState = this.ActiveMdiChild == null ? FormWindowState.Normal : this.ActiveMdiChild.WindowState; // must get this value before creating a new form (see end of note 2 at top of file)

            if (sequenceForm == null) // sequenceFile is not in a form, make a new form
            {
                sequenceForm = new SequenceForm(this, this.axApplicationMgr, file);
                this.mChildFormsKeyedByViewMgr.Add(sequenceForm.GetViewMgr(), sequenceForm); // use GetOcx to return the activeX control instead of the wrapper because that is what axApplicationMgr.SequenceFileViewMgr returns, and we need to compare apples to apples
                SetNewChildFormLocation(sequenceForm);
            }
            sequenceForm.DisplaySequenceFile();
            sequenceForm.Show();
            sequenceForm.WindowState = newWindowState; // see end of Note 2 at top of file
            sequenceForm.Activate();
        }

        void SetNewChildFormLocation(Form childForm)
        {
            Rectangle proposedBounds = childForm.Bounds;
            proposedBounds.Location = this.mNewChildFormInitialLocation;

            if (this.ClientRectangle.Contains(proposedBounds))
                this.mNewChildFormInitialLocation.Offset(MainForm.NewChildFormOffsetX, MainForm.NewChildFormOffsetY); 	// offset the next window from this one
            else
            {	// wrap back to origin
                proposedBounds.Location = new Point(0, 0);
                this.mNewChildFormInitialLocation = new Point(MainForm.NewChildFormOffsetX, MainForm.NewChildFormOffsetY);
            }

            // strangely, even though we set the bounds before hand, the form still briefly flashes at the origin before jumping to right place when it is shown. We see this apparent .NET bug because we can't use the default MDI form positioning. See Note 2 at the top of the file.
            childForm.StartPosition = FormStartPosition.Manual;
            childForm.Bounds = proposedBounds;
        }

        private Form FindForm(object executionOrSequenceFile)
        {
            if (executionOrSequenceFile is SequenceFile)
                return findFormBySequenceFile(executionOrSequenceFile as SequenceFile);
            else
                if (executionOrSequenceFile is Execution)
                    return findFormByExecution(executionOrSequenceFile as Execution);
                else
                    return null;
        }

        private SequenceForm findFormByExecution(Execution execution)
        {
            // find the view manager that displays this execution
            Object viewMgr = this.axApplicationMgr.GetExecutionViewMgr(execution);

            // return the form on which the view manager resides
            if (viewMgr == null)
                return null;
            else
                return ((SequenceForm)this.mChildFormsKeyedByViewMgr[viewMgr]);
        }

        // returns the form that displays the sequence file. returns null if no form displays the sequence file
        private SequenceForm findFormBySequenceFile(SequenceFile sequenceFile)
        {
            // find the view manager that displays this execution
            Object viewMgr = this.axApplicationMgr.GetSequenceFileViewMgr(sequenceFile);

            // return the form on which the view manager resides
            if (viewMgr == null)
                return null;
            else
                return ((SequenceForm)this.mChildFormsKeyedByViewMgr[viewMgr]);
        }


        private void axApplicationMgr_QueryCloseExecution(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_QueryCloseExecutionEvent e)
        {
            Object viewMgr = this.axApplicationMgr.GetExecutionViewMgr(e.exec);
            if (viewMgr != null)
                this.mChildFormsKeyedByViewMgr.Remove(viewMgr);
        }

        private void axApplicationMgr_QueryCloseSequenceFile(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_QueryCloseSequenceFileEvent e)
        {
            Object viewMgr = this.axApplicationMgr.GetSequenceFileViewMgr(e.file);
            if (viewMgr != null)
                this.mChildFormsKeyedByViewMgr.Remove(viewMgr);
        }


        // handle menu item Window>>Cascade
        private void cascadeMenuItem_Click(object sender, System.EventArgs e)
        {
            CascadeWindows(this.MdiChildren);
        }

        // handle menu item Window>>Tile Vertical
        private void tileMenuItem_Click(object sender, System.EventArgs e)
        {
            TileWindows(this.MdiChildren);
        }

        // handle menu item Window>>Tile Horizontal
        private void tileHorizontalMenuItem_Click(object sender, System.EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        // handle menu item Window>>Tile Vertical
        private void tileVerticalmenuItem_Click(object sender, System.EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        // handle selected TestStand UIMessages
        private void axApplicationMgr_UIMessageEvent(object sender, NationalInstruments.TestStand.Interop.UI.Ax._ApplicationMgrEvents_UIMessageEventEvent e)
        {
            switch (e.uiMsg.Event)
            {
                case UIMessageCodes.UIMsg_TileWindows:		// tile the windows the message specifies				
                    TileWindows(GetForms(e.uiMsg));
                    break;
                case UIMessageCodes.UIMsg_CascadeWindows:	// cascade the windows the message specifies
                    CascadeWindows(GetForms(e.uiMsg));
                    break;
            }
        }

        // we needed our own tile method because the Form.LayoutMdi method doesn't do simultaneous horizontal and vertical tiling and it doesn't tile a given subset of child windows
        private void TileWindows(ICollection forms)
        {
            Tiler tiler = new Tiler();

            // the -4's are fudge factors needed to prevent scrollbars from appearing
            Rectangle adjustedBounds = new Rectangle(this.ClientRectangle.X, this.ClientRectangle.Y, this.ClientRectangle.Width - 4, this.ClientRectangle.Height - this.axStatusBar.Height - 4);

            tiler.TileWindows(forms, adjustedBounds);
        }

        // we needed our own cascade method because the Form.LayoutMdi method doesn't cascade a given subset of child windows
        private void CascadeWindows(ICollection forms)
        {
            ArrayList formsReordered = new ArrayList(forms);
            Point location = new Point(0, 0);
            int minCascades = 7;	// try to fit this many forms before wrapping back to starting location

            // put the active form, if any, at the end of the list of forms to cascade
            if (this.ActiveMdiChild != null && formsReordered.Contains(this.ActiveMdiChild))
            {
                formsReordered.RemoveAt(formsReordered.IndexOf(this.ActiveMdiChild));
                formsReordered.Add(this.ActiveMdiChild);
            }

            IEnumerator i = formsReordered.GetEnumerator();

            // cascade all the forms in the collection
            while (i.MoveNext())
            {
                Form form = (Form)i.Current;

                // adjust size down to a minumum if necessary to try to fit within the parent form
                form.Height = Math.Min(form.Height, Math.Max(this.ClientRectangle.Height - minCascades * MainForm.NewChildFormOffsetY, 200));
                form.Width = Math.Min(form.Width, Math.Max(this.ClientRectangle.Width - minCascades * MainForm.NewChildFormOffsetX, 300));

                // compute new location
                Rectangle bounds = form.Bounds;
                bounds.Location = location;

                // does it fit?
                if (this.ClientRectangle.Contains(bounds))
                    location.Offset(NewChildFormOffsetX, NewChildFormOffsetY);	// yes, adjust cascade location for next form
                else
                {	// no, restart cascading from the origin
                    location.Y = 0;
                    location.X = 0;
                    bounds.Location = location;
                }

                // apply new location
                form.Location = bounds.Location;
                form.BringToFront();
            }
        }

        // returns the forms that message specifies. 
        private ICollection GetForms(UIMessage uiMsg)
        {
            PropertyObject array = uiMsg.ActiveXData as PropertyObject;
            ArrayList forms = new ArrayList();

            switch (uiMsg.Event)
            {
                case UIMessageCodes.UIMsg_TileWindows:
                    goto case UIMessageCodes.UIMsg_CascadeWindows;
                case UIMessageCodes.UIMsg_CascadeWindows:
                    if (array != null)
                    {
                        int numElements = array.GetNumElements();

                        for (int index = 0; index < numElements; index++)
                        {
                            Form form = FindForm(array.GetValInterfaceByOffset(index, 0));
                            if (form != null)
                                forms.Add(form);
                        }
                    }
                    else 	// a null ActiveXData property means "all windows"
                        return this.MdiChildren;
                    break;
            }

            return forms;
        }

        private void MainForm_MdiChildActivate(object sender, System.EventArgs e)
        {
            // Rebind Process Model status bar pane to correct manager for active window
            
            object activeMgr = GetActiveMgr();

            if (activeMgr is ApplicationMgr)
                ((ApplicationMgr)activeMgr).ConnectCaption(this.axStatusBar.Panes["ProcessModel"], CaptionSources.CaptionSource_CurrentProcessModelFile, false);
            else
                if (activeMgr is SequenceFileViewMgr)
                    ((SequenceFileViewMgr)activeMgr).ConnectCaption(this.axStatusBar.Panes["ProcessModel"], CaptionSources.CaptionSource_CurrentProcessModelFile, false);
                else
                    if (activeMgr is ExecutionViewMgr)
                        ((ExecutionViewMgr)activeMgr).ConnectCaption(this.axStatusBar.Panes["ProcessModel"], CaptionSources.CaptionSource_CurrentProcessModelFile, false);
        }

        // Another .NET MDI workaround. Strangely, when you click the main form close box, the active child form (and only the active one) is immediately closed before you get 
        // any other event, including the close of the main form! This conflicts with the ApplicationMgr.ReloadSequenceFilesOnStart = ReloadFile_All setting because before the
        // application manager has a chance to record which files to open on the next launch, one of the files has already been closed. To fix this, we take over the close box handling
        // ourselves by trapping the corresponding window message and then closing the main form first.
        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_CLOSE = 0xF060;
            if (m.Msg == WM_SYSCOMMAND && m.WParam.ToInt32() == SC_CLOSE)
            {
                CancelEventArgs e = new CancelEventArgs();
                OnClosing(e);
                if (e.Cancel)
                    return;
            }
            else if (m.Msg == WM_QUERYENDSESSION)
            {
                // set the sessionEnding flag so I will know the form is closing because the user
                // is trying to shutdown, restart, or logoff windows
                sessionEnding = true;
                Application.Exit();
            }

            base.WndProc(ref m);
        }

        private object GetActiveMgr()
        {
            object mgr = this.axApplicationMgr.GetOcx();

            if (this.ActiveMdiChild != null)
            {
                foreach (DictionaryEntry entry in this.mChildFormsKeyedByViewMgr)
                {
                    if (entry.Value == this.ActiveMdiChild)
                    {
                        mgr = entry.Key;
                        break;
                    }
                }
            }

            return mgr;
        }

        public void axApplicationMgr_EditModeChanged(object sender, EventArgs e)
        {
            //if editor mode is disabled, hide the insertion palette pane.  if editor mode is enabled, hide it
            foreach (DictionaryEntry i in mChildFormsKeyedByViewMgr)
            {
                SequenceForm sequenceForm = (SequenceForm)i.Value;
                if (sequenceForm.isSequenceViewType())
                    sequenceForm.setEditMode();
            }

        }

        private void tileVerticalMenuItem_Click(object sender, EventArgs e)
        {

        }


 	}
}


